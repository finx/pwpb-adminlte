<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = ['total', 'bayar', 'kembalian'];

    public function transactionItems(){
        return $this->hasMany(TransactionItem::class, 'transaction_id', 'id');
    }
}
